
var margins = {"left": 80, "right": 10, "top": 20, "bottom": 10};

/// http://data.okfn.org/data/core/s-and-p-500-companies#data

function createParallelCoordinates(placement, width, height, dimensionFilter) {
    var x = d3.scale.ordinal().rangePoints([0, width], 1),
        y = {},
        dragging = {};

    var line = d3.svg.line(),
        axis = d3.svg.axis().orient("left"),
        background,
        foreground;

    var ordinals = [];
    var quantitatives = [];

    d3.select(placement).select('svg').remove();
    var svg = d3.select(placement).append("svg")
        .attr("width", width + margins.left + margins.right)
        .attr("height", height + margins.top + margins.bottom)
        .append("g")
        .attr("transform", "translate(" + margins.left + "," + margins.top + ")");

    // fire an async request for the financials data in CSV format
    d3.csv("assets/data/financials.csv", function(error, moneys) {
        // Extract the list of dimensions and create a scale for each.
        x.domain(dimensions = d3.keys(moneys[0]).filter(function(d) {
            // ignore some dimensions completely
            if(d == "Symbol" || d == "Name" || 
                (typeof dimensionFilter !== 'undefined' && dimensionFilter.indexOf(d) == -1))
                return;
            // make a judgement on the type of scale based on the first row
            var isQuantitative = !isNaN(moneys[0][d]);
            if(isQuantitative) {
                quantitatives.push(d);  // remember the quantitative dimensions
                return (y[d] = d3.scale.linear()
                    .domain(d3.extent(moneys, function(p) { return +p[d]; }))
                    .range([height, 0]));
            }
            else {
                ordinals.push(d);       // remember the ordinal dimensions
                return (y[d] = d3.scale.ordinal()
                    .domain(d3.set(moneys.map(function(row) { return row[d];})).values().sort())
                    .rangePoints([height, 0])); // rangePoints/rangeBands etc are required for ordinal scales!
            }
        }));

        // add grey background lines for unbrushed data during brushing
        background = svg.append("g")
            .attr("class", "background pc")
            .selectAll("path")
            .data(moneys)
            .enter().append("path")
            .attr("d", path);

        // add blue foreground lines for both brushing highlights and normal unbrushed display
        foreground = svg.append("g")
            .attr("class", "foreground pc")
            .selectAll("path")
            .data(moneys)
            .enter().append("path")
            .attr("d", path);

        // add a group element for each dimension.
        var g = svg.selectAll(".dimension")
            .data(dimensions)
            .enter().append("g")
            .attr("class", "pc dimension")
            .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
            .call(d3.behavior.drag()
            .origin(function(d) { return {x: x(d)}; })
            .on("dragstart", function(d) {
                dragging[d] = x(d);
                background.attr("visibility", "hidden");
            })
            .on("drag", function(d) {
                dragging[d] = Math.min(width, Math.max(0, d3.event.x));
                foreground.attr("d", path);
                dimensions.sort(function(a, b) { return position(a) - position(b); });
                x.domain(dimensions);
                g.attr("transform", function(d) { return "translate(" + position(d) + ")"; })
            })
            .on("dragend", function(d) {
                delete dragging[d];
                transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
                transition(foreground).attr("d", path);
            
                background
                    .attr("d", path)
                    .transition()
                    .delay(500)
                    .duration(0)
                    .attr("visibility", null);
            }));

        // Add an axis and title.
        g.append("g")
            .attr("class", "pc axis")
            .each(function(d) { d3.select(this).call(axis.scale(y[d])); })
            .append("text")
            .style("text-anchor", "middle")
            .attr("y", -9)
            .text(function(d) { return d; });

        // Add and store a brush for each axis.
        g.append("g")
            .attr("class", "pc brush")
            .each(function(d) {
                d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
            })
        .selectAll("rect")
            .attr("x", -8)
            .attr("width", 16);
    });

    function position(d) {
        var v = dragging[d];
        return v == null ? x(d) : v;
    }

    function transition(g) {
        return g.transition().duration(500);
    }

    // Returns the path for a given data point.
    function path(d) {
        return line(dimensions.map(function(p) { return [position(p), y[p](d[p])]; }));
    }

    function brushstart() {
        d3.event.sourceEvent.stopPropagation();
    }

    // Handles a brush event, toggling the display of foreground lines.
    function brush() {
        var activeOrdinals = ordinals.filter(function(p) { return !y[p].brush.empty(); });
        var activeQuantitatives = quantitatives.filter(function(p) { return !y[p].brush.empty(); })

        var ordExtents = activeOrdinals.map(function(p) { 
                return y[p].domain().filter(function(d) {
                    return (y[p].brush.extent()[0] <= y[p](d)) && (y[p](d) <= y[p].brush.extent()[1]); 
                }); 
            });
        var quantExtents = activeQuantitatives.map(function(p) { return y[p].brush.extent(); });
        
        foreground.style("display", function(d) {
            return activeQuantitatives.map(function(p, i) {
                return quantExtents[i][0] <= d[p] && d[p] <= quantExtents[i][1];
            }).concat(activeOrdinals.map(function(p, i) {
                return ordExtents[i].indexOf(d[p]) != -1;
            })).every(function(d) { return d; }) ? null : "none";
        });
    }
}
